package com.wichell.framework.wechat.bo.response;

/**   
 * @ClassName:  WechatOauth2AcessToken   
 * @Description: 网页授权凭证对象
 * @author: Isaac
 * @date:   2018年2月11日 下午1:53:42   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public class WechatOauth2AccessToken extends BaseWechatResponse {
	/**   
	 * @Fields access_token : 网页授权接口调用凭证
	 */
	private String access_token;

	/**   
	 * @Fields expires_in : 凭证超时时间   
	 */
	private Integer expires_in;

	/**   
	 * @Fields refresh_token : 用户刷新access_token
	 */
	private String refresh_token;

	/**   
	 * @Fields openid : 用户唯一标识，请注意，在未关注公众号时，用户访问公众号的网页，也会产生一个用户和公众号唯一的OpenID
	 */
	private String openid;

	/**   
	 * @Fields scope : 用户授权的作用域，使用逗号（,）分隔
	 */
	private String scope;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public Integer getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(Integer expires_in) {
		this.expires_in = expires_in;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	@Override
	public String toString() {
		return "WechatOauth2AccessToken [access_token=" + access_token + ", expires_in=" + expires_in
				+ ", refresh_token=" + refresh_token + ", openid=" + openid + ", scope=" + scope + ", getErrorCode()="
				+ getErrorCode() + ", getErrMsg()=" + getErrMsg() + "]";
	}

}

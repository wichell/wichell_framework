package com.wichell.framework.util;

public interface BeanUtilFilter<T, V> {
	public T filter(V source, T target);
}
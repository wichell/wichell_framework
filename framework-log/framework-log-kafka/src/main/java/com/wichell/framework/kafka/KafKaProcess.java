package com.wichell.framework.kafka;

import com.wichell.framework.bean.MsgBean;

public interface KafKaProcess {
	public void process(MsgBean msgBean);
}

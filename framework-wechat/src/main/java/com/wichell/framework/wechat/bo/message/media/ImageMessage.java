package com.wichell.framework.wechat.bo.message.media;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.wichell.framework.util.xml.CDATAConvert;

@XStreamAlias("xml")
public class ImageMessage extends MediaMessage {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = -3063314976392233911L;

	/**   
	 * @Fields picUrl : 图片链接（由系统生成）
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("PicUrl")
	private String picUrl;

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

}

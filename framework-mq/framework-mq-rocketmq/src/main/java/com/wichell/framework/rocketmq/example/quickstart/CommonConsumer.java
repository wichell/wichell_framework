/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wichell.framework.rocketmq.example.quickstart;

import java.util.List;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageConst;
import org.apache.rocketmq.common.message.MessageExt;

/**
 * This example shows how to subscribe and consume messages using providing {@link DefaultMQPushConsumer}.
 */
public class CommonConsumer {
	// private static Logger logger =
	// LoggerFactory.getLogger(CommonConsumer.class);

	public static void main(String[] args) throws InterruptedException, MQClientException {

		/*
		 * Instantiate with specified consumer group name.
		 */
		DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("commonConsumer");

		/*
		 * Specify name server addresses.
		 * <p/>
		 *
		 * Alternatively, you may specify name server addresses via exporting environmental variable: NAMESRV_ADDR
		 * <pre>
		 * {@code
		 * consumer.setNamesrvAddr("name-server1-ip:9876;name-server2-ip:9876");
		 * }
		 * </pre>
		 */
		consumer.setNamesrvAddr("10.138.61.57:9876;");
		/*
		 * Specify where to start in case the specified consumer group is a brand new one.
		 */
		consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);

		/*
		 * Subscribe one more more topics to consume.
		 */
		consumer.subscribe("TopicCommon", "*");

		/*
		 *  Register callback to execute on arrival of messages fetched from brokers.
		 */
		consumer.registerMessageListener(new MessageListenerConcurrently() {

			@Override
			public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
				try {
					MessageExt messageExt = msgs.get(0);
					System.out.println(" Receive New Messages: " + msgs + "-- body：" + new String(messageExt.getBody())
							+ "--重试次数：" + messageExt.getReconsumeTimes() + "--消息offset：" + messageExt.getQueueOffset()
							+ "--最大偏移量：" + messageExt.getProperty(MessageConst.PROPERTY_MAX_OFFSET) + "--uniquekey:"
							+ messageExt.getProperty(MessageConst.PROPERTY_UNIQ_CLIENT_MESSAGE_ID_KEYIDX));
					;
					if (messageExt.getReconsumeTimes() < 3)
						return ConsumeConcurrentlyStatus.RECONSUME_LATER;
				} catch (Exception e) {
					e.printStackTrace();
				}
				context.setDelayLevelWhenNextConsume(2); // 设置重试机制是手动还是自动
				return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;

				/*   MessageExt msg = msgs.get(0);
				  try {
				String topic = msg.getTopic();
				String msgBody = new String(msg.getBody(),"utf-8");
				String tags = msg.getTags();
				System.out.println("get massage : " + " topic : " + topic + " tags : " + tags + " msg : " +msgBody);
				  }catch (Exception e){
				e.printStackTrace();
				return ConsumeConcurrentlyStatus.CONSUME_SUCCESS; //requeue 一会再消费
				  }
				  	 return ConsumeConcurrentlyStatus.CONSUME_SUCCESS; // response broker ack
				
				 */
			}
		});

		/*
		 *  Launch the consumer instance.
		 */
		consumer.start();

		System.out.printf("Common Consumer1 Started.%n");
	}
}

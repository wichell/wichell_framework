package com.wichell.framework.wechat.bo.response;

import java.io.Serializable;

public class WechatJsConfig implements Serializable {
	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = 1562695023825244052L;
	/**   
	 * @Fields appId : appId
	 */
	private String appId;
	/**   
	 * @Fields timestamp : 生成签名的时间戳
	 */
	private String timestamp;
	/**   
	 * @Fields nonceStr : 生成签名的随机串
	 */
	private String nonceStr;
	/**   
	 * @Fields signature : 签名
	 */
	private String signature;

	public WechatJsConfig() {
		super();
	}

	public WechatJsConfig(String appId, String timestamp, String nonceStr, String signature) {
		super();
		this.appId = appId;
		this.timestamp = timestamp;
		this.nonceStr = nonceStr;
		this.signature = signature;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
}

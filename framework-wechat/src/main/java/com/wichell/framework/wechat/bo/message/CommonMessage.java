package com.wichell.framework.wechat.bo.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class CommonMessage extends WechatMessage {
	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = -882187567738970849L;
	/**   
	 * @Fields msgId : 消息id，64位整型
	 */
	@XStreamAlias("MsgId")
	private String msgId;

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

}

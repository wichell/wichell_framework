package com.wichell.framework.rocketmq.example.pull;

import org.apache.rocketmq.client.consumer.MQPullConsumer;
import org.apache.rocketmq.client.consumer.MQPullConsumerScheduleService;
import org.apache.rocketmq.client.consumer.PullResult;
import org.apache.rocketmq.client.consumer.PullTaskCallback;
import org.apache.rocketmq.client.consumer.PullTaskContext;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

public class PullConsumerSchedule {
	public static void main(String[] args) throws MQClientException {

		final MQPullConsumerScheduleService scheduleService = new MQPullConsumerScheduleService("pullConsumerSchedule");

		scheduleService.getDefaultMQPullConsumer().setNamesrvAddr("10.138.61.59:9876;10.138.61.57:9876");

		scheduleService.setMessageModel(MessageModel.CLUSTERING);

		scheduleService.registerPullTaskCallback("TopicPull", new PullTaskCallback() {

			@Override

			public void doPullTask(MessageQueue mq, PullTaskContext context) {

				MQPullConsumer consumer = context.getPullConsumer();

				try {

					// 获取从哪里拉取

					long offset = consumer.fetchConsumeOffset(mq, false);

					if (offset < 0) {
						offset = 0;
					}

					PullResult pullResult = consumer.pull(mq, "*", offset, 32);

					System.out.println(offset + "\t" + mq + "\t" + pullResult);

					switch (pullResult.getPullStatus()) {

					case FOUND:
						for (MessageExt messageExt : pullResult.getMsgFoundList()) {
							System.out.println(
									"msg:" + messageExt + "--body:" + new String(messageExt.getBody(), "UTF-8"));
						}

						break;

					case NO_MATCHED_MSG:

						break;

					case NO_NEW_MSG:

					case OFFSET_ILLEGAL:

						break;

					default:

						break;

					}

					// 存储Offset，客户端每隔5s会定时刷新到Broker

					consumer.updateConsumeOffset(mq, pullResult.getNextBeginOffset());

					// 设置再过100ms后重新拉取

					context.setPullNextDelayTimeMillis(1000);

				}

				catch (Exception e) {

					e.printStackTrace();

				}

			}

		});

		scheduleService.start();

	}

}

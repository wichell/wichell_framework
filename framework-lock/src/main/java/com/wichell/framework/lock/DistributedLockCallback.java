package com.wichell.framework.lock;

/**
 * @ClassName: DistributedLockCallback
 * @Description:分布式锁回调接口
 * @author: Isaac
 * @date: 2018年1月18日 上午11:57:10
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public interface DistributedLockCallback<T> {

	/**   
	 * @Title: process   
	 * @Description: 调用者必须在此方法中实现需要加分布式锁的业务逻辑
	 * @param: @return      
	 * @return: T      
	 * @throws   
	 */
	T process();

	/**   
	 * @Title: getLockName   
	 * @Description: 得到分布式锁名称 
	 * @param: @return      
	 * @return: String      
	 * @throws   
	 */
	String getLockName();
}

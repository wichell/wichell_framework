package com.wichell.framework.wechat.util;

import java.text.MessageFormat;

import com.wichell.framework.wechat.bo.response.WechatAccessToken;
import com.wichell.framework.wechat.bo.response.WechatOauth2AccessToken;
import com.wichell.framework.wechat.contants.WechatContants;
import com.wichell.framework.wechat.exception.WechatRequestException;

/**   
 * @ClassName:  AccessTokenUtils   
 * @Description:Token工具类
 * @author: Isaac
 * @date:   2018年2月12日 上午9:57:54   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public final class AccessTokenUtils {

	/**   
	 * @Title: getPublicAccessToken   
	 * @Description: 获取全局AccessToken
	 * @param: @param appId
	 * @param: @param appSecret
	 * @param: @return
	 * @param: @throws WechatRequestException      
	 * @return: WechatAccessToken      
	 * @throws   
	 */
	public static WechatAccessToken getPublicAccessToken(String appId, String appSecret) throws WechatRequestException {
		String tokenUrl = MessageFormat.format(WechatContants.TOKENURL, appId, appSecret);
		return WechatUtils.request(tokenUrl);
	}

	/**   
	 * @Title: getOauthAccessToken   
	 * @Description: 获取网页授权AccessToken
	 * @param: @param appId
	 * @param: @param appSecret
	 * @param: @param code
	 * @param: @return
	 * @param: @throws WechatRequestException      
	 * @return: WechatOauth2AccessToken      
	 * @throws   
	 */
	public static WechatOauth2AccessToken getOauthAccessToken(String appId, String appSecret, String code)
			throws WechatRequestException {
		String oauthTokenUrl = MessageFormat.format(WechatContants.OAUTHTOKENURL, appId, appSecret, code);
		return WechatUtils.request(oauthTokenUrl);
	}

}

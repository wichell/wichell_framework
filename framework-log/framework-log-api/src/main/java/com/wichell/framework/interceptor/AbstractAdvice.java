package com.wichell.framework.interceptor;

import org.aopalliance.intercept.MethodInterceptor;

import com.wichell.framework.util.JacksonUtil;

public abstract class AbstractAdvice implements MethodInterceptor {

	public String paramToString(Object args[]) {
		StringBuffer sb = new StringBuffer();
		for (Object arg : args) {
			try {
				sb.append(JacksonUtil.obj2Json(arg)).append(";");
			} catch (Exception e) {
				sb.append(arg);
			}
		}
		return sb.toString();
	}
}

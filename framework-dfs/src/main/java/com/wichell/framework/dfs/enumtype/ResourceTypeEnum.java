package com.wichell.framework.dfs.enumtype;

/**   
 * @ClassName:  ResourceTypeEnum   
 * @Description:资源类型
 * @author: Isaac
 * @date:   2018年3月2日 下午12:06:42   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public enum ResourceTypeEnum {
	IMG(1, "images"), AUDIO(2, "audios"), FILE(3, "files");

	private int code;
	private String desc;

	ResourceTypeEnum(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public static ResourceTypeEnum valueOf(Integer code) {
		switch (code) {
		case 1: {
			return ResourceTypeEnum.IMG;
		}
		case 2: {
			return ResourceTypeEnum.AUDIO;
		}
		case 3: {
			return ResourceTypeEnum.FILE;
		}

		default: {
			return null;
		}
		}
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getMediaType() {
		String result = "";
		switch (code) {
		case 1: {
			result = "image/jpeg";
			break;
		}

		default: {
			result = "text/html";
			break;
		}
		}
		return result;
	}
}

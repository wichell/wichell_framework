package com.wichell.framework.util;

import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

/**
 * 权限URL拦截
 * 
 * @author sk_mao
 *
 */
public class AntUrlPathMatcher implements UrlMatcher {
	private boolean requiresLowerCaseUrl = true;
	private PathMatcher pathMatcher = new AntPathMatcher();

	public AntUrlPathMatcher() {
		this(true);
	}

	public AntUrlPathMatcher(boolean requiresLowerCaseUrl) {
		this.requiresLowerCaseUrl = requiresLowerCaseUrl;
	}

	@Override
	public Object compile(String path) {
		if (requiresLowerCaseUrl) {
			return path.toLowerCase();
		}

		return path;
	}

	public void setRequiresLowerCaseUrl(boolean requiresLowerCaseUrl) {
		this.requiresLowerCaseUrl = requiresLowerCaseUrl;
	}

	@Override
	public boolean pathMatchesUrl(Object path, String url) {
		if (path == null) {
			return true;
		}
		return pathMatcher.match((String) path, url);
	}

	@Override
	public String getUniversalMatchPattern() {
		return "/**";
	}

	@Override
	public boolean requiresLowerCaseUrl() {
		return requiresLowerCaseUrl;
	}

	@Override
	public String toString() {
		return getClass().getName() + "[requiresLowerCase='" + requiresLowerCaseUrl + "']";
	}
}

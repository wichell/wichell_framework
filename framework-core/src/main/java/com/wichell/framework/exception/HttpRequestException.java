package com.wichell.framework.exception;

/**   
 * @ClassName:  HttpRequestException   
 * @Description:Http请求异常 
 * @author: Isaac
 * @date:   2018年2月22日 上午11:18:25   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public class HttpRequestException extends BaseException {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = 1672823478059406845L;

	public HttpRequestException(String errorMessage) {
		super(errorMessage);
	}

}

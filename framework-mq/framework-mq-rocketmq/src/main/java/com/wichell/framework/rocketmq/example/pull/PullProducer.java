package com.wichell.framework.rocketmq.example.pull;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

public class PullProducer {
	public static void main(String[] args) throws MQClientException, InterruptedException {

		DefaultMQProducer producer = new DefaultMQProducer("pullProducer");

		producer.setNamesrvAddr("10.138.61.59:9876;10.138.61.57:9876");

		producer.start();

		for (int i = 0; i < 10; i++) {

			try {

				Message msg = new Message("TopicPull", // topic

						"TagA", // tag

						("pull Hello RocketMQ " + i).getBytes()// body

				);

				SendResult sendResult = producer.send(msg);

				System.out.println(sendResult);

				// Thread.sleep(6000);

			}

			catch (Exception e) {

				e.printStackTrace();

				// Thread.sleep(3000);

			}

		}

		producer.shutdown();

	}

}

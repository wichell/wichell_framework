package com.wichell.framework.wechat.bo.message.media;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.wichell.framework.util.xml.CDATAConvert;

@XStreamAlias("xml")
public class VoiceMessage extends MediaMessage {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = -5080862926717374554L;

	/**   
	 * @Fields format : 语音格式，如amr，speex等
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("Format")
	private String format;

	/**   
	 * @Fields recognition : 语音识别结果，UTF8编码
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("Recognition")
	private String recognition;

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getRecognition() {
		return recognition;
	}

	public void setRecognition(String recognition) {
		this.recognition = recognition;
	}

}

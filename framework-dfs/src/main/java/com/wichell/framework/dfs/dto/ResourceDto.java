package com.wichell.framework.dfs.dto;

import java.io.Serializable;

public class ResourceDto implements Serializable {
	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = -6251695919743938508L;

	/**   
	 * @Fields resourceCode : 资源Code
	 */
	private String resourceCode;

	/**   
	 * @Fields orignalName : 原始文件名
	 */
	private String orignalName;

	private byte[] resourceContent;

	/**   
	 * @Fields resourceContentBase64 : 资源内容Base64
	 */
	private String resourceContentBase64;

	public ResourceDto() {
		super();
	}

	public ResourceDto(String resourceCode, String orignalName, byte[] resourceContent, String resourceContentBase64) {
		super();
		this.resourceCode = resourceCode;
		this.orignalName = orignalName;
		this.resourceContent = resourceContent;
		this.resourceContentBase64 = resourceContentBase64;
	}

	public String getResourceCode() {
		return resourceCode;
	}

	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}

	public String getResourceContentBase64() {
		return resourceContentBase64;
	}

	public void setResourceContentBase64(String resourceContentBase64) {
		this.resourceContentBase64 = resourceContentBase64;
	}

	public byte[] getResourceContent() {
		return resourceContent;
	}

	public void setResourceContent(byte[] resourceContent) {
		this.resourceContent = resourceContent;
	}

	public String getOrignalName() {
		return orignalName;
	}

	public void setOrignalName(String orignalName) {
		this.orignalName = orignalName;
	}

}

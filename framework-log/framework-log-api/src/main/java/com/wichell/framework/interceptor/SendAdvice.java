package com.wichell.framework.interceptor;

import com.wichell.framework.bean.MsgBean;

public interface SendAdvice {

	/**   
	 * @Title: addMessage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param msg      
	 * @return: void      
	 * @throws   
	 */
	void addMessage(MsgBean msg);
}

package com.wichell.framework.wechat.bo.message.event;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.wichell.framework.util.xml.CDATAConvert;

/**   
 * @ClassName:  SubscribeEventMessage   
 * @Description:用户未关注时，进行关注后的事件推送
 * @author: Isaac
 * @date:   2018年2月12日 下午2:45:52   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
@XStreamAlias("xml")
public class SubscribeEventMessage extends EventMessage {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = -7884736712622336953L;

	/**   
	 * @Fields Ticket : 二维码的ticket，可用来换取二维码图片
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("Ticket")
	private String ticket;

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

}

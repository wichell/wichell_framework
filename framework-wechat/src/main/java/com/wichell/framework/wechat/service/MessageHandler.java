package com.wichell.framework.wechat.service;

import com.wichell.framework.wechat.exception.WechatMessageException;

/**   
 * @ClassName:  MessageHandler   
 * @Description:消息处理服务
 * @author: Isaac
 * @date:   2018年2月12日 下午1:59:54   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public interface MessageHandler {

	/**   
	 * @Title: processCustomMessage   
	 * @Description: 处理非法的消息
	 * @param: @param eventType
	 * @param: @param message
	 * @param: @return      
	 * @return: String      
	 * @throws   
	 */
	String handleCustomMessage(String message) throws WechatMessageException;
}

package com.wichell.framework.wechat.bo.message.media;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.wichell.framework.util.xml.CDATAConvert;

@XStreamAlias("xml")
public class VideoMessage extends MediaMessage {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = 3247225539092805555L;

	/**   
	 * @Fields thumbMediaId : 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("ThumbMediaId")
	private String thumbMediaId;

	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

}

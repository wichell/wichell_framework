package com.wichell.framework.wechat.contants;

/**   
 * @ClassName:  WechatContants   
 * @Description:微信常量包  
 * @author: Isaac
 * @date:   2018年2月9日 下午2:08:01   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public class WechatContants {
	/**   
	 * @Fields TOKENURL : 获取access_token的URL
	 */
	public static final String TOKENURL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}";

	/**   
	 * @Fields OAUTHTOKENURL : 获取oauth_access_token的URL
	 */
	public static final String OAUTHTOKENURL = " https://api.weixin.qq.com/sns/oauth2/access_token?appid={0}&secret={1}&code={2}&grant_type=authorization_code";

	/**   
	 * @Fields MESSAGE_EVENT : 消息类型事件
	 */
	public static final String MESSAGE_EVENT = "EVENT";

	/**   
	 * @Fields JSAPITICKETURL : 获得jsapi_ticket（有效期7200秒，开发者必须在自己的服务全局缓存jsapi_ticket）
	 */
	public static final String JSAPITICKETURL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={0}&type=jsapi";

	/**   
	 * @Fields CREATEQRCODETICKETURL : 创建二维码ticket的URL
	 */
	public static final String CREATEQRCODETICKETURL = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={0}";

	/**   
	 * @Fields GETQRCODEURL : 通过ticket换取二维码的URL
	 */
	public static final String GETQRCODEURL = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={0}";
}

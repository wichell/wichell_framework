package com.wichell.framework.wechat.util;

import java.text.MessageFormat;

import com.wichell.framework.wechat.bo.response.WechatJsApiTicket;
import com.wichell.framework.wechat.contants.WechatContants;
import com.wichell.framework.wechat.exception.WechatRequestException;

/**   
 * @ClassName:  JsApiUtils   
 * @Description:微信jsapi工具类
 * @author: Isaac
 * @date:   2018年2月13日 下午2:08:17   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public class JsApiUtils {
	/**   
	 * @Title: geJsApiTicket   
	 * @Description: 获取api_ticket
	 * @param: @param token
	 * @param: @return
	 * @param: @throws WechatRequestException      
	 * @return: WechatJsApiTicket      
	 * @throws   
	 */
	public static WechatJsApiTicket geJsApiTicket(String token) throws WechatRequestException {
		String jsTicketUrl = MessageFormat.format(WechatContants.JSAPITICKETURL, token);
		return WechatUtils.request(jsTicketUrl);
	}
}

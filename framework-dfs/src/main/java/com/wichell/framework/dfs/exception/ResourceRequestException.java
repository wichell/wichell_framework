package com.wichell.framework.dfs.exception;

import com.wichell.framework.exception.BaseException;

/**   
 * @ClassName:  ResourceRequestException   
 * @Description:资源请求异常 
 * @author: Isaac
 * @date:   2018年3月2日 下午12:13:47   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public class ResourceRequestException extends BaseException {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = 7680388808039436585L;

	public ResourceRequestException(String errorMessage) {
		super(errorMessage);
	}

}

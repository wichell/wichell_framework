package com.wichell.framework.wechat.exception;

import com.wichell.framework.exception.BaseException;

public class WechatRequestException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3525572402230691809L;

	public WechatRequestException(String errorMessage) {
		super(errorMessage);
	}

}

package com.wichell.framework.wechat.bo.message.event;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.wichell.framework.util.xml.CDATAConvert;
import com.wichell.framework.wechat.bo.message.WechatMessage;

/**   
 * @ClassName:  EventMessage   
 * @Description:事件消息基类
 * @author: Isaac
 * @date:   2018年2月12日 下午2:49:16   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
@XStreamAlias("xml")
public class EventMessage extends WechatMessage {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = 3612018574839529321L;

	/**   
	 * @Fields event : 事件类型，subscribe
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("Event")
	private String event;

	/**   
	 * @Fields eventKey : 事件KEY值，qrscene_为前缀，后面为二维码的参数值
	 */
	@XStreamAlias("EventKey")
	private String eventKey;

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

}

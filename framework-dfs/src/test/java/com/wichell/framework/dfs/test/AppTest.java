package com.wichell.framework.dfs.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.wichell.framework.dfs.dto.ResourceDto;
import com.wichell.framework.dfs.exception.ResourceNotFoundException;
import com.wichell.framework.dfs.exception.ResourceRequestException;
import com.wichell.framework.dfs.service.ResourceService;

public class AppTest extends BaseJunit4Test {

	@Autowired
	private ResourceService resourceService;

	@Test
	public void test() {
		FileInputStream fileInputStream;
		try {
			fileInputStream = new FileInputStream("C:/a.jpg");
			String aaa = resourceService.upload("a.jpg", fileInputStream);
			ResourceDto dto = resourceService.download(aaa);

			System.out.println("data:image/jpeg;base64," + dto.getResourceContentBase64());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ResourceRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ResourceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

package com.wichell.framework.wechat.bo.message;

import javax.xml.bind.annotation.XmlElement;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.wichell.framework.util.xml.CDATAConvert;

@XStreamAlias("xml")
public class LocationMessage extends CommonMessage {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = 5324783043007042610L;

	/**   
	 * @Fields locationX : 地理位置维度
	 */
	@XStreamAlias("Location_X")
	private String locationX;

	/**   
	 * @Fields locationY : 地理位置经度
	 */
	@XStreamAlias("Location_Y")
	private String locationY;

	/**   
	 * @Fields scale : 	地图缩放大小
	 */
	@XmlElement(name = "Scale")
	private String scale;

	/**   
	 * @Fields label : 地理位置信息
	 */
	@XStreamConverter(CDATAConvert.class)
	@XmlElement(name = "Label")
	private String label;

	public String getLocationX() {
		return locationX;
	}

	public void setLocationX(String locationX) {
		this.locationX = locationX;
	}

	public String getLocationY() {
		return locationY;
	}

	public void setLocationY(String locationY) {
		this.locationY = locationY;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}

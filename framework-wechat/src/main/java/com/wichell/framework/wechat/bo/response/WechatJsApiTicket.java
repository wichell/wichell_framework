package com.wichell.framework.wechat.bo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WechatJsApiTicket extends BaseWechatResponse {

	/**   
	 * @Fields ticket : api_ticket，卡券接口中签名所需凭证
	 */
	private String ticket;

	/**   
	 * @Fields expires : 有效时间
	 */
	@JsonProperty("expires_in")
	private Integer expires;

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public Integer getExpires() {
		return expires;
	}

	public void setExpires(Integer expires) {
		this.expires = expires;
	}

}

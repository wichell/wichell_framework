package com.wichell.framework.wechat.bo.message;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.wichell.framework.util.xml.CDATAConvert;

public abstract class WechatMessage implements Serializable {
	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = 5363933997081679840L;
	/**   
	 * @Fields toUserName : 开发者微信号
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("ToUserName")
	private String toUserName;
	/**   
	 * @Fields fromUserName : 发送方帐号（一个OpenID）
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("FromUserName")
	private String fromUserName;
	/**   
	 * @Fields createTime : 消息创建时间 （整型）
	 */
	@XStreamAlias("CreateTime")
	private Integer createTime;
	/**   
	 * @Fields msgType : 消息类型枚举
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("MsgType")
	private String msgType;

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public Integer getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Integer createTime) {
		this.createTime = createTime;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

}

package com.wichell.framework.spring;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.wichell.framework.interceptor.ExceptionHandler;
import com.wichell.framework.interceptor.KafKaSendAdvice;
import com.wichell.framework.interceptor.LogAdvice;
import com.wichell.framework.kafka.KafKaProcessListener;
import com.wichell.framework.redis.RedisService;
import com.wichell.framework.rocketmq.client.RocketMQCommonProducer;

public class NamespaceHandler extends NamespaceHandlerSupport {
	@Override
	public void init() {
		registerBeanDefinitionParser("kafka-provider", new BeanDefinitionParser(KafKaSendAdvice.class));
		registerBeanDefinitionParser("kafka-consumer", new BeanDefinitionParser(KafKaProcessListener.class));
		registerBeanDefinitionParser("redis", new BeanDefinitionParser(RedisService.class));
		registerBeanDefinitionParser("exceptionHandler", new BeanDefinitionParser(ExceptionHandler.class));
		registerBeanDefinitionParser("logAdvice",
				new AopBeanDefinitionParser(LogAdvice.class,
						new AopBeanDefinition("datalogInsertPointCut",
								"@annotation(com.wichell.framework.annotation.Log)", "pointcut", null, null),
						new AopBeanDefinition("logAdvisor", null, "advisor", "LogAdvice", "datalogInsertPointCut")));
		// registerBeanDefinitionParser("tcc", new
		// AopBeanDefinitionParser(OpenTransactionalMethodInvok.class,new
		// AopBeanDefinition("openTransactionalPointCut",
		// "@annotation(com.alibaba.dubbo.tcc.transactional.anno.OpenTran)",
		// "pointcut",null, null),
		// new AopBeanDefinition("openTransactionalAdvisor", null, "advisor",
		// "OpenTransactionalMethodInvok","openTransactionalPointCut")));
		registerBeanDefinitionParser("quartz", new BeanDefinitionParser(SchedulerFactoryBean.class));
		registerBeanDefinitionParser("rocketmq-common-provider",
				new BeanDefinitionParser(RocketMQCommonProducer.class));

	}
}

package com.wichell.framework.wechat.bo.message.media;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.wichell.framework.util.xml.CDATAConvert;
import com.wichell.framework.wechat.bo.message.CommonMessage;

public abstract class MediaMessage extends CommonMessage {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = -6099638118689443329L;

	/**   
	 * @Fields mediaId : 消息媒体id，可以调用多媒体文件下载接口拉取数据。
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("MediaId")
	private String mediaId;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

}

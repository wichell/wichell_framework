package com.wichell.framework.wechat.enums;

/**   
 * @ClassName:  MsgTypeEnum   
 * @Description:消息类型枚举
 * @author: Isaac
 * @date:   2018年2月12日 上午10:23:28   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public enum MsgTypeEnum {
	/**   
	 * @Fields TEXT : 文本
	 */
	TEXT,
	/**   
	 * @Fields MUSIC : 音乐
	 */
	MUSIC,
	/**   
	 * @Fields NEWS : 图文
	 */
	NEWS,
	/**   
	 * @Fields IMAGES : 图片
	 */
	IMAGES,
	/**   
	 * @Fields LINKS : 链接
	 */
	LINKS,
	/**   
	 * @Fields VOICE : 音频
	 */
	VOICE,
	/**   
	 * @Fields VIDEO : 视频
	 */
	VIDEO,
	/**   
	 * @Fields SHORTVIDEO : 小视频
	 */
	SHORTVIDEO,
	/**   
	 * @Fields LOCATION : 地理位置
	 */
	LOCATION,
	/**   
	* @Fields EVENT : 推送
	*/
	EVENT,
	/**   
	 * @Fields TRANSFER_CUSTOMER_SERVICE : 多客服
	 */
	TRANSFER_CUSTOMER_SERVICE;
}

package com.wichell.framework.wechat.exception;

import com.wichell.framework.exception.BaseException;

public class WechatJsApiException extends BaseException {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = 6943450166851577859L;

	public WechatJsApiException(String errorMessage) {
		super(errorMessage);
	}

}

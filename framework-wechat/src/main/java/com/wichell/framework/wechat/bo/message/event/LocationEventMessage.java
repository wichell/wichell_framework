package com.wichell.framework.wechat.bo.message.event;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**   
 * @ClassName:  LocationEventMessage   
 * @Description:上报地理位置
 * @author: Isaac
 * @date:   2018年2月12日 下午2:47:41   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
@XStreamAlias("xml")
public class LocationEventMessage extends EventMessage {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = 7794316107607954347L;

	/**   
	 * @Fields Latitude : 地理位置纬度
	 */
	@XStreamAlias("Latitude")
	private String latitude;

	/**   
	 * @Fields Longitude : 地理位置经度
	 */
	@XStreamAlias("Longitude")
	private String longitude;

	/**   
	 * @Fields Precision : 地理位置精度
	 */
	@XStreamAlias("LatitPrecisionude")
	private String precision;

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPrecision() {
		return precision;
	}

	public void setPrecision(String precision) {
		this.precision = precision;
	}

}

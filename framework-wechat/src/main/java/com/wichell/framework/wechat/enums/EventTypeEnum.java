package com.wichell.framework.wechat.enums;

/**   
 * @ClassName:  EventTypeEnum   
 * @Description:微信消息事件枚举
 * @author: Isaac
 * @date:   2018年2月12日 上午10:14:32   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public enum EventTypeEnum {
	/**   
	 * @Fields SUBSCRIBE : 关注  
	 */
	SUBSCRIBE,
	/**   
	* @Fields UNSUBSCRIBE : 取消关注
	*/
	UNSUBSCRIBE,
	/**   
	 * @Fields SCAN : 扫描带参数二维码事件
	 */
	SCAN,
	/**   
	 * @Fields CLICK : CLICK(自定义菜单点击事件)
	 */
	CLICK;
}

package com.wichell.framework.wechat.bo.message;

import javax.xml.bind.annotation.XmlElement;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.wichell.framework.util.xml.CDATAConvert;

@XStreamAlias("xml")
public class LinkMessage extends CommonMessage {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = -4102701313731432540L;

	/**   
	 * @Fields title : 消息标题
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("Title")
	private String title;

	/**   
	 * @Fields description : 消息描述
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("Description")
	private String description;

	/**   
	 * @Fields url : 消息链接
	 */
	@XStreamConverter(CDATAConvert.class)
	@XmlElement(name = "Url")
	private String url;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}

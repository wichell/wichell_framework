package com.wichell.framework.util.xml;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**   
 * @ClassName:  CDATAConvert   
 * @Description:处理JAVA对象转换成XML时添加<!CDATA[ ]]>标签 
 * @author: Isaac
 * @date:   2018年2月22日 下午2:45:40   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public class CDATAConvert implements Converter {
	private static final String PREFIX_CDATA = "<![CDATA[";
	private static final String SUFFIX_CDATA = "]]>";

	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class type) {
		return String.class.isAssignableFrom(type);
	}

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
		String trans = PREFIX_CDATA + source + SUFFIX_CDATA;
		writer.setValue(trans);
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		return reader.getValue();
	}
}

package com.wichell.framework.proxy.mybatis;

import com.wichell.framework.proxy.ProxyInterceptor;

public interface IMybatisProxyFactory {
	public ProxyInterceptor[] init();
}

package com.wichell.framework.lock;

import java.util.concurrent.TimeUnit;

/**   
 * @ClassName:  DistributedLockTemplate   
 * @Description:分布式锁操作模板   
 * @author: Isaac
 * @date:   2018年1月18日 下午1:53:58   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public interface DistributedLockTemplate {

	/**   
	 * @Title: lock   
	 * @Description: 使用分布式锁，使用锁默认超时时间。 
	 * @param: @param callback
	 * @param: @return      
	 * @return: T      
	 * @throws   
	 */
	public <T> T lock(DistributedLockCallback<T> callback);

	/**   
	 * @Title: lock   
	 * @Description: 使用分布式锁。自定义锁的超时时间
	 * @param: @param callback
	 * @param: @param leaseTime 锁超时时间。超时后自动释放锁。 
	 * @param: @param timeUnit
	 * @param: @return      
	 * @return: T      
	 * @throws   
	 */
	public <T> T lock(DistributedLockCallback<T> callback, long leaseTime, TimeUnit timeUnit);
}

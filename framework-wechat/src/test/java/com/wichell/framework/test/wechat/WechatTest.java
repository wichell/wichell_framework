package com.wichell.framework.test.wechat;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.wichell.framework.wechat.bo.response.WechatAccessToken;
import com.wichell.framework.wechat.service.WechatRequestService;

public class WechatTest extends BaseJunit4Test {

	@Autowired
	private WechatRequestService wechatRequestService;

	@Test
	public void test() throws Exception {
		WechatAccessToken accessToken = wechatRequestService.getPublicAccessToken();
		System.out.println("result:" + accessToken.toString());
	}
}

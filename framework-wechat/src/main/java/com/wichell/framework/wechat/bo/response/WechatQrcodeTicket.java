package com.wichell.framework.wechat.bo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WechatQrcodeTicket extends BaseWechatResponse {

	/**   
	 * @Fields ticket : 获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。
	 */
	private String ticket;

	/**   
	 * @Fields expires : 该二维码有效时间，以秒为单位。 最大不超过2592000（即30天）。
	 */
	@JsonProperty("expire_seconds")
	private Integer expires;

	/**   
	 * @Fields url : 二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片
	 */
	private String url;

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public Integer getExpires() {
		return expires;
	}

	public void setExpires(Integer expires) {
		this.expires = expires;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}

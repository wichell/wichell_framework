package com.wichell.framework.lock;

import java.util.concurrent.TimeUnit;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

/**   
 * @ClassName:  RedissonDistributedLockTemplate   
 * @Description:Redis实现分布式锁  
 * @author: Isaac
 * @date:   2018年1月18日 下午12:11:59   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public class RedissonDistributedLockTemplate implements DistributedLockTemplate {

	private static final long DEFAULT_TIMEOUT = 5;
	private static final TimeUnit DEFAULT_TIME_UNIT = TimeUnit.SECONDS;

	private RedissonClient redisson;

	public RedissonDistributedLockTemplate() {
	}

	public RedissonDistributedLockTemplate(RedissonClient redisson) {
		this.redisson = redisson;
	}

	@Override
	public <T> T lock(DistributedLockCallback<T> callback) {
		return lock(callback, DEFAULT_TIMEOUT, DEFAULT_TIME_UNIT);
	}

	@Override
	public <T> T lock(DistributedLockCallback<T> callback, long leaseTime, TimeUnit timeUnit) {
		RLock lock = null;
		try {
			lock = redisson.getLock(callback.getLockName());
			lock.lock(leaseTime, timeUnit);
			return callback.process();
		} finally {
			if (lock != null) {
				lock.unlock();
			}
		}
	}

	public void setRedisson(RedissonClient redisson) {
		this.redisson = redisson;
	}

}

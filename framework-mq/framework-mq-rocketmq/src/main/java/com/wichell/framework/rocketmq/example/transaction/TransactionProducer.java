package com.wichell.framework.rocketmq.example.transaction;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.TransactionCheckListener;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.common.message.Message;

public class TransactionProducer {
	public static void main(String[] args) throws MQClientException, InterruptedException {
		TransactionCheckListener transactionCheckListener = new TransactionCheckListenerImpl();
		TransactionMQProducer producer = new TransactionMQProducer("transactionProducer");
		producer.setNamesrvAddr("10.138.61.59:9876;10.138.61.57:9876");
		producer.setCheckThreadPoolMinSize(2);
		producer.setCheckThreadPoolMaxSize(2);
		producer.setCheckRequestHoldMax(2000);
		producer.setTransactionCheckListener(transactionCheckListener);
		producer.start();

		// String[] tags = new String[] {"TagA"};
		TransactionExecuterImpl tranExecuter = new TransactionExecuterImpl();
		for (int i = 0; i < 10; i++) {
			try {
				Message msg = new Message("TopicTransaction", "TagA", "KEY" + i,
						("Hello RocketTransactionMQ " + i).getBytes());
				SendResult sendResult = producer.sendMessageInTransaction(msg, tranExecuter, null);
				System.out.println("producer:" + msg);

				// new
				// TransactionCheckListenerImpl().checkLocalTransactionState(new
				// MessageExt(null, null, null, null, null,
				// sendResult.getMsgId()));
				System.out.println("TransactionProducer result---" + sendResult);

				Thread.sleep(10);
			} catch (MQClientException e) {
				e.printStackTrace();
			}
		}

		for (int i = 0; i < 100000; i++) {
			Thread.sleep(1000);
		}
		producer.shutdown();
	}

}

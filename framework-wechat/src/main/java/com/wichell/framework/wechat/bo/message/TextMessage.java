package com.wichell.framework.wechat.bo.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.wichell.framework.util.xml.CDATAConvert;

@XStreamAlias("xml")
public class TextMessage extends CommonMessage {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = -5162093215357758579L;

	/**   
	 * @Fields content : 文本消息
	 */
	@XStreamConverter(CDATAConvert.class)
	@XStreamAlias("Content")
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}

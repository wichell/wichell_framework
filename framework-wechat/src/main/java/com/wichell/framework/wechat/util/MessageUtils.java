package com.wichell.framework.wechat.util;

import com.wichell.framework.util.XmlUtil;

/**   
 * @ClassName:  MessageUtils   
 * @Description:微信转发消息工具类
 * @author: Isaac
 * @date:   2018年2月12日 上午9:57:34   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public class MessageUtils {

	public static String createCommonMessage(Object data) {
		return XmlUtil.toXml(data);
	}

	public static <T> T readMessage(String message, Class<T> clazz) {
		return XmlUtil.toBean(message, clazz);
	}
}

package com.wichell.framework.wechat.exception;

import com.wichell.framework.exception.BaseException;

public class WechatMessageException extends BaseException {

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */
	private static final long serialVersionUID = -939808406044435047L;

	public WechatMessageException(String errorMessage) {
		super(errorMessage);
	}

}

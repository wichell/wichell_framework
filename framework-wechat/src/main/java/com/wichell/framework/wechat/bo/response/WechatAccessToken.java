package com.wichell.framework.wechat.bo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**   
 * @ClassName:  WechatAccessToken   
 * @Description:基础支持AccessToken对象({"access_token":"ACCESS_TOKEN","expires_in":7200})
 * @author: Isaac
 * @date:   2018年2月9日 下午2:50:16   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public class WechatAccessToken extends BaseWechatResponse {

	/**   
	 * @Fields access_token : TODO(用一句话描述这个变量表示什么)   
	 */
	@JsonProperty("access_token")
	private String accessToken;

	/**   
	 * @Fields expires_in : TODO(用一句话描述这个变量表示什么)   
	 */
	@JsonProperty("expires_in")
	private String expires;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getExpires() {
		return expires;
	}

	public void setExpires(String expires) {
		this.expires = expires;
	}

	@Override
	public String toString() {
		return "WechatAccessToken [accessToken=" + accessToken + ", expires=" + expires + ", getErrorCode()="
				+ getErrorCode() + ", getErrMsg()=" + getErrMsg() + "]";
	}

}

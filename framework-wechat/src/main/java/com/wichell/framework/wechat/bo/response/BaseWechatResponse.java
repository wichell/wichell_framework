package com.wichell.framework.wechat.bo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**   
 * @ClassName:  BaseWechatResponse   
 * @Description:微信请求响应基类（{"errcode":40013,"errmsg":"invalid appid"}） 
 * @author: Isaac
 * @date:   2018年2月9日 下午2:51:27   
 *     
 * @Copyright: 2018 www.wichell.com Inc. All rights reserved.
 */
public class BaseWechatResponse {
	/**   
	 * @Fields errorCode : 错误编码 
	 */
	@JsonProperty("errorcode")
	private String errorCode;
	/**   
	 * @Fields errMsg : 错误报错  
	 */
	@JsonProperty("errmsg")
	private String errMsg;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

}
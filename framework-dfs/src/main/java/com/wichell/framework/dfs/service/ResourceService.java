package com.wichell.framework.dfs.service;

import java.io.InputStream;

import org.springframework.beans.factory.DisposableBean;

import com.wichell.framework.dfs.dto.ResourceDto;
import com.wichell.framework.dfs.exception.ResourceNotFoundException;
import com.wichell.framework.dfs.exception.ResourceRequestException;

public interface ResourceService extends DisposableBean {
	/**   
	 * @Title: uploadResource   
	 * @Description: 上传资源
	 * @param: @param resourceName 资源名称
	 * @param: @param resourceContent 资源内容
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: ResourceDto      
	 * @throws   
	 */
	String upload(String resourceName, InputStream resourceContent) throws ResourceRequestException;

	/**   
	 * @Title: download   
	 * @Description: 根据资源code去请求资源
	 * @param: @param resourceCode
	 * @param: @return
	 * @param: @throws ResourceRequestException      
	 * @param: @throws ResourceNotFoundException      
	 * @return: ResourceDto      
	 * @throws   
	 */
	ResourceDto download(String resourceCode) throws ResourceRequestException, ResourceNotFoundException;
}

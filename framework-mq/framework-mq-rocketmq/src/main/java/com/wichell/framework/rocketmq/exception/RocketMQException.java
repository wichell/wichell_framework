package com.wichell.framework.rocketmq.exception;
/**RokcetMQ异常*/
public class RocketMQException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3485125636434778859L;
	
	public RocketMQException(String errorMessage) {
		super(errorMessage);
	}
}
